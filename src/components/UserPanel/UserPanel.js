import React from "react";
import firebase from "../../firebase";
import { Grid, Header, Icon, Dropdown } from "semantic-ui-react";

class UserPanel extends React.Component {
    dropdownOptions = () => [
        {
            key: "userKey",
            text: (
                <span>
          Signed in as <strong>User</strong>
        </span>
            ),
            disabled: true
        },
        {
            key: "avatarKey",
            text: <span>Change Avatar</span>
        },
        {
            key: "logoutKey",
            text: <span onClick={this.handleSignout}>Logout</span>
        }
    ];

    handleSignout = () => {
        firebase
            .auth()
            .signOut()
            .then(() => console.log("signed out!"));
    };

    render() {
        return (
            <Grid style={{ background: "black" }}>
                <Grid.Column>
                    <Grid.Row style={{ padding: "1.2em", margin: 0 }}>
                        {/* App Header */}
                        <Header inverted floated="left" as="h2">
                            <Icon name="code" />
                            <Header.Content style={{color: "AntiqueWhite"}}>HassonWeb</Header.Content>
                        </Header>
                    </Grid.Row>

                    {/* User Dropdown  */}
                    <Header style={{ padding: "0.25em" }} as="h4" inverted>
                        <Dropdown
                            trigger={<span>User</span>}
                            options={this.dropdownOptions()}
                        />
                    </Header>
                </Grid.Column>
            </Grid>
        );
    }
}

export default UserPanel;
