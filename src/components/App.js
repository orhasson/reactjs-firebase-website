import React from "react";
import { Grid } from "semantic-ui-react";
import "./App.css";

import ColorPanel from "./ColorPanel/ColorPanel";
import SidePanel from "./SidePanel/SidePanel";
import Messages from "./Messages/Messages";
import PanelArea from "./PanelArea/PanelArea";
import backVideo from "../resources/I_Just_Wanted.mp4";

const App = () => (

  <Grid columns="equal" className="app" >

    <ColorPanel />
    <SidePanel />

    <Grid.Column style={{ marginLeft: 320 }}>
      <Messages />
    </Grid.Column>

    <Grid.Column width={4}>
      <PanelArea />
    </Grid.Column>
      <video id="background-video" loop autoPlay>
          <source src={backVideo} type="video/mp4"/>
      </video>
  </Grid>

);

export default App;
